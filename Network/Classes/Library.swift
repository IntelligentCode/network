//
//  Library.swift
//  Network
//
//  Created by Артём Шляхтин on 25/01/2017.
//  Copyright © 2017 Артём Шляхтин. All rights reserved.
//

import Foundation

public struct NetworkFramework {
    public static let bundleName = "ru.roseurobank.Network"
}

// MARK: - Structs

public struct MultiPartData {
    
    /** Имя файла */
    public var name: String
    
    /** Файл */
    public var data: Data
    
    /** Mime тип файла */
    public var mimeType: String
    
    public init(name: String, mimeType: String, data: Data) {
        self.name = name
        self.mimeType = mimeType
        self.data = data
    }
}

public struct NetworkPacket {
    public var url: URL
    public var body: Data?
    public var method: NetworkHTTPMethod = .GET
    public var headers: [AnyHashable: Any]?
    public var certificateHost: CFString?
    
    // MARK: - Init
    
    public init(url: URL, body: Data? = nil) {
        self.url = url
        self.body = body
    }
    
    public init(url: URL, parts: [MultiPartData]) {
        self.url = url
        let boundary = joinData(parts)
        updateHeaders(boundary)
    }
    
    // MARK: - Actions
    
    mutating func joinData(_ parts: [MultiPartData]) -> String {
        let data = NSMutableData()
        let boundary = "Boundary-\(UUID().uuidString)"
        for part in parts {
            data.appendString("--\(boundary)\r\n")
            data.appendString("Content-Disposition: form-data; name=\"file\"; filename=\"\(part.name)\"\r\n")
            data.appendString("Content-Type: \(part.mimeType)\r\n\r\n")
            data.append(part.data)
            data.appendString("\r\n")
        }
        data.appendString("--\(boundary)--")
        self.body = data as Data
        return boundary
    }
    
    mutating func updateHeaders(_ boundary: String) {
        guard let data = body else { return }
        headers = ["Content-Type":"multipart/form-data; boundary=\(boundary)", "Content-Length": data.count]
    }
    
}


// MARK: - Protocols

public enum NetworkHTTPMethod: String {
    case OPTIONS, GET, HEAD, POST, PUT, PATCH, DELETE, TRACE, CONNECT
}

public typealias NetworkOperationResult = (success: Bool, networkRequestOperation: NetworkRequestOperation, response: URLResponse?, data: Data?, error: Error?)

