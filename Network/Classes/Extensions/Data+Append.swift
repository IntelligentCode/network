//
//  Data+Append.swift
//  Network
//
//  Created by Артём Шляхтин on 17/03/2017.
//  Copyright © 2017 Artem Shlyakhtin. All rights reserved.
//

import Foundation

extension NSMutableData {
    
    func appendString(_ string: String) {
        let data = string.data(using: String.Encoding.utf8, allowLossyConversion: false)
        append(data!)
    }
    
}
