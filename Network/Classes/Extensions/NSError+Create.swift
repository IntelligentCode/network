//
//  NSError+Create.swift
//  Network
//
//  Created by Артём Шляхтин on 17/03/2017.
//  Copyright © 2017 Artem Shlyakhtin. All rights reserved.
//

import Foundation

extension NSError {

    class func create(_ code: Int, desc: String) -> NSError {
        let dict = [NSLocalizedDescriptionKey: desc]
        let error = NSError(domain: NetworkFramework.bundleName, code: code, userInfo: dict)
        return error
    }
    
}
