//
//  NetworkIP.m
//  Network
//
//  Created by Артем Шляхтин on 01.12.15.
//  Copyright © 2015 Артем Шляхтин. All rights reserved.
//

#import "NetworkTools.h"
#include <ifaddrs.h>
#include <arpa/inet.h>

@implementation NetworkTools

+ (NSArray *)getIPAddresses
{
    NSMutableArray *addresses = [NSMutableArray array];
    struct ifaddrs *interfaces = NULL;
    struct ifaddrs *currentAddress = NULL;
    
    int success = getifaddrs(&interfaces);
    if (success == 0) {
        currentAddress = interfaces;
        while(currentAddress != NULL) {
            if(currentAddress->ifa_addr->sa_family == AF_INET) {
                NSString *address = [NSString stringWithUTF8String:inet_ntoa(((struct sockaddr_in *)currentAddress->ifa_addr)->sin_addr)];
                if (![address isEqual:@"127.0.0.1"]) {
                    [addresses addObject:address];
                }
            }
            currentAddress = currentAddress->ifa_next;
        }
    }
    freeifaddrs(interfaces);
    return addresses;
}

@end
