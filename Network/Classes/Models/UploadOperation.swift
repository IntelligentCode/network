//
//  UploadOperation.swift
//  Network
//
//  Created by Артём Шляхтин on 17/03/2017.
//  Copyright © 2017 Artem Shlyakhtin. All rights reserved.
//

import UIKit
import System

class UploadOperation: NetworkRequestOperation {
    
    // MARK: - Lifecycle
    
    override init(packet: NetworkPacket, completionBlock aCompleted: (() -> Void)? = nil) {
        super.init(packet: packet, completionBlock: aCompleted)
        self.name = "upload operation"
    }
    
    // MARK: - Actions
    
    override func start() {
        super.start()
        
        guard let file = body else {
            let error = NSError.create(-100, desc: "Тело запроса пустое.")
            failure(error, forTask: nil)
            return
        }
        
        let config = prepareConfiguration()
        let request = prepareRequest()
        let session = URLSession(configuration: config, delegate: self, delegateQueue: delegateQueue)
        let task = session.uploadTask(with: request, from: file)
        task.resume()
    }
    
}

extension UploadOperation: URLSessionDataDelegate {
    
    func urlSession(_ session: URLSession, task: URLSessionTask, didCompleteWithError error: Error?) {
        if let exception = error {
            failure(exception, forTask: task)
            session.invalidateAndCancel()
            return
        }
        
        currentQueue.addOperation {
            let result = NetworkOperationResult(true, self, task.response, nil, nil)
            self.blockResponse?(result)
            self.finish()
        }
        
        session.finishTasksAndInvalidate()
    }
    
}
