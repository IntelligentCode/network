//
//  RequestOperation.swift
//  Network
//
//  Created by Артём Шляхтин on 17/03/2017.
//  Copyright © 2017 Artem Shlyakhtin. All rights reserved.
//

import UIKit

class RequestOperation: NetworkRequestOperation {
    
    /** Тело пакета */
    fileprivate var data = NSMutableData()

    // MARK: - Lifecycle
    
    override init(packet: NetworkPacket, completionBlock aCompleted: (() -> Void)? = nil) {
        super.init(packet: packet, completionBlock: aCompleted)
        self.name = "request operation"
    }
    
    // MARK: - Actions
    
    override func start() {
        super.start()
        
        let config = prepareConfiguration()
        let request = prepareRequest()
        let session = URLSession(configuration: config, delegate: self, delegateQueue: delegateQueue)
        let task = session.dataTask(with: request)
        
        task.resume()
    }
    
}

extension RequestOperation: URLSessionDataDelegate {
    
    func urlSession(_ session: URLSession, dataTask: URLSessionDataTask, didReceive response: URLResponse, completionHandler: @escaping (URLSession.ResponseDisposition) -> Void) {
        self.data.setData(Data())
        completionHandler(.allow)
    }
    
    func urlSession(_ session: URLSession, dataTask: URLSessionDataTask, didReceive data: Data) {
        self.data.append(data)
        if isCancelled { dataTask.cancel() }
    }
    
    func urlSession(_ session: URLSession, task: URLSessionTask, didCompleteWithError error: Error?) {
        if let exception = error {
            failure(exception, forTask: task)
            session.invalidateAndCancel()
            return
        }
        
        currentQueue.addOperation {
            let result = NetworkOperationResult(true, self, task.response, self.data as Data, nil)
            self.blockResponse?(result)
            self.finish()
        }
        
        session.finishTasksAndInvalidate()
    }
    
}
