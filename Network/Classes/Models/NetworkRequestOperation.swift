//
//  NetworkOperation.swift
//  Network
//
//  Created by Артём Шляхтин on 25/01/2017.
//  Copyright © 2017 Артём Шляхтин. All rights reserved.
//

import UIKit
import System

public class NetworkRequestOperation: OperationTemplate {
    
    /** URL Адрес */
    public let url: URL
    
    /** Тело пакета */
    public let body: Data?
    
    /** Метод передачи данных */
    public let method: NetworkHTTPMethod
    
    /** Block возвращающий результат */
    var blockProgress: ((_ bytesSent: Int64, _ totalBytesSent: Int64, _ totalBytesSent: Int64) -> Void)?
    
    /** Block возвращающий процесс передачи данных */
    var blockResponse: ((_ result: NetworkOperationResult) -> Void)?
    
    let currentQueue: OperationQueue
    let delegateQueue = OperationQueue()
    fileprivate let certificateHost: CFString?
    fileprivate var headers: [AnyHashable: Any]?
    
    // MARK: - Lifecycle
    
    /**
     Инициализация операции
     
     - parameter packet: Пакет содержащий необходимую информацию для работы NetworkOperation.
     - parameter delegate: Протокол описывающий методы, которые необходимо объявить для работы с NetworkOperation.
     - parameter completionBlock: Блок запускается после того, как операция завершит свою работу.
    */
    public init(packet: NetworkPacket, completionBlock aCompleted: (() -> Void)? = nil) {
        self.url = packet.url
        self.body = packet.body
        self.method = packet.method
        self.headers = packet.headers
        self.certificateHost = packet.certificateHost
        self.currentQueue = (OperationQueue.current == nil) ? OperationQueue.main : OperationQueue.current!
        
        super.init(completionBlock: aCompleted, finishInParent: false)
        self.name = "network operation"
        self.delegateQueue.name = "delegate_queue"
    }
    
    deinit {
    }
    
    // MARK: - Actions
    
    public override func start() {
        super.start()
    }
    
    public func response(_ response: @escaping (NetworkOperationResult) -> Void) -> NetworkRequestOperation {
        self.blockResponse = response
        return self
    }
    
    public func progress(_ progress: @escaping (Int64, Int64, Int64) -> Void) -> NetworkRequestOperation {
        self.blockProgress = progress
        return self
    }
    
    func failure(_ error: Error, forTask task: URLSessionTask?) {
        currentQueue.addOperation({
            let result = NetworkOperationResult(false, self, task?.response, nil, error)
            self.blockResponse?(result)
            self.finish()
        })
    }
    
    // MARK: - Configurations
    
    func prepareConfiguration() -> URLSessionConfiguration {
        let sessionConfiguration = URLSessionConfiguration.default
        sessionConfiguration.httpAdditionalHeaders = headers
        return sessionConfiguration
    }
    
    func prepareRequest() -> URLRequest {
        let request = NSMutableURLRequest(url: url)
        switch method {
            case .POST:
                request.httpMethod = method.rawValue
                request.httpBody = body
            default: break
        }
        return request as URLRequest
    }
    
}

// MARK: - Calculate Properties

extension NetworkRequestOperation {
    
    public var bytes: Int64 {
        get {
            var total: Int64 = 0
            if let data = body {
                total += Int64(data.count)
            }
            return total
        }
    }
    
}

extension NetworkRequestOperation: URLSessionTaskDelegate {
    
    public func urlSession(_ session: URLSession, didReceive challenge: URLAuthenticationChallenge, completionHandler: @escaping (URLSession.AuthChallengeDisposition, URLCredential?) -> Void) {
        guard let serverTrust = challenge.protectionSpace.serverTrust else {
            completionHandler(.cancelAuthenticationChallenge, nil)
            return
        }
        
        let policies = NSMutableArray()
        policies.add(SecPolicyCreateSSL(true, certificateHost))
        SecTrustSetPolicies(serverTrust, policies)
        
        var result: SecTrustResultType = .deny
        SecTrustEvaluate(serverTrust, &result)
        let isServerTrusted: Bool = (result == .unspecified || result == .proceed)
        
        if isServerTrusted {
            let credential = URLCredential(trust: serverTrust)
            completionHandler(.useCredential, credential)
        } else {
            completionHandler(.cancelAuthenticationChallenge, nil)
        }
    }
    
    public func urlSession(_ session: URLSession, task: URLSessionTask, didSendBodyData bytesSent: Int64, totalBytesSent: Int64, totalBytesExpectedToSend: Int64) {
        if isCancelled { task.cancel() }
        currentQueue.addOperation({
            self.blockProgress?(bytesSent, totalBytesSent, totalBytesExpectedToSend)
        })
    }

}
