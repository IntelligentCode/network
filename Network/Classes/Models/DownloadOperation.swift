//
//  DownloadOperation.swift
//  Network
//
//  Created by Артём Шляхтин on 20/03/2017.
//  Copyright © 2017 Artem Shlyakhtin. All rights reserved.
//

import UIKit

//TODO:
// - Разработать загрузку в Background
// - Разработать докачку файла

class DownloadOperation: NetworkRequestOperation {
    
    /** Тело пакета */
    fileprivate var data: Data?
    
    // MARK: - Lifecycle
    
    override init(packet: NetworkPacket, completionBlock aCompleted: (() -> Void)? = nil) {
        super.init(packet: packet, completionBlock: aCompleted)
        self.name = "download operation"
    }
    
    // MARK: - Actions
    
    override func start() {
        super.start()
        
        let config = prepareConfiguration()
        let request = prepareRequest()
        let session = URLSession(configuration: config, delegate: self, delegateQueue: delegateQueue)
        let task = session.downloadTask(with: request)
        task.resume()
    }
}

extension DownloadOperation: URLSessionDownloadDelegate {

    func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didWriteData bytesWritten: Int64, totalBytesWritten: Int64, totalBytesExpectedToWrite: Int64) {
        if isCancelled { downloadTask.cancel() }
        currentQueue.addOperation {
            self.blockProgress?(bytesWritten, totalBytesWritten, totalBytesExpectedToWrite)
        }
    }
    
    func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didFinishDownloadingTo location: URL) {
        self.data = try? Data(contentsOf: location)
    }
    
    func urlSession(_ session: URLSession, task: URLSessionTask, didCompleteWithError error: Error?) {
        if let exception = error {
            failure(exception, forTask: task)
            session.invalidateAndCancel()
            return
        }
        
        currentQueue.addOperation {
            let result = NetworkOperationResult(true, self, task.response, self.data, nil)
            self.blockResponse?(result)
            self.finish()
        }
        
        session.finishTasksAndInvalidate()
    }

    
}
