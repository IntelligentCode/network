//
//  NetworkOperation.swift
//  Network
//
//  Created by Артём Шляхтин on 20/03/2017.
//  Copyright © 2017 Artem Shlyakhtin. All rights reserved.
//

import UIKit

public class NetworkOperation: NSObject {

    public class func download(_ aPacket: NetworkPacket) -> NetworkRequestOperation {
        let operation = DownloadOperation(packet: aPacket)
        return operation
    }
    
    public class func upload(_ aPacket: NetworkPacket) -> NetworkRequestOperation {
        let operation = UploadOperation(packet: aPacket)
        return operation
    }
    
    public class func request(_ aPacket: NetworkPacket) -> NetworkRequestOperation {
        let operation = RequestOperation(packet: aPacket)
        return operation
    }
    
}
