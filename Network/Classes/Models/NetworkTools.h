//
//  NetworkIP.h
//  Network
//
//  Created by Артем Шляхтин on 01.12.15.
//  Copyright © 2015 Артем Шляхтин. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NetworkTools : NSObject

+ (NSArray *)getIPAddresses;

@end
